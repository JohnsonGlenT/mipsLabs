	.text
	#================ add function ================
add:
	add $v0,$a0,$a1
	jr $ra

	#================ mult function ================
mult:

	sub $sp,4
	sw $ra,0($sp)
	move $t1,$a1
	li $t0,0
	li $a1,0
	mloop:
	jal add
	move $a1,$v0
	addi $t0,$t0,1
	bne $t0,$t1,mloop

	lw $ra,0($sp)
	add $sp,4

	jr $ra

	#================ exp function ================
exp:

	sub $sp,4
	sw $ra,0($sp)
	move $t9,$a1
	move $t6,$a0
	li $v0,1
	li $t8,0
	eloop:
	move $a1,$t6
	move $a0,$v0

	jal mult
	addi $t8,$t8,1
	bne $t9,$t8,eloop

	lw $ra,0($sp)
	add $sp,4

	jr $ra

	############################ WARNING #########################
	# CS 333 STUDENTS SHOULD NOT MODIFY ANY CODE BELOW THIS POINT#
	##############################################################

#================main program================
main:

	# prompt user and read first number
	la $a0,pleaseTypeFirst
	jal printString
	jal readNum
	move $s0,$v0

	# prompt user and read second number
	la $a0,pleaseTypeSecond
	jal printString
	jal readNum
	move $s1,$v0

	# call add, storing result in $s2
	move $a0,$s0
	move $a1,$s1
	jal add
	move $s2,$v0

	# call mult, storing result in $s3
	move $a0,$s0
	move $a1,$s1
	jal mult
	move $s3,$v0

	# call exp, storing result in $s4
	move $a0,$s0
	move $a1,$s1
	jal exp
	move $s4,$v0

	# print sum
	la $a0,sumIs
	jal printString
	move $a0,$s2
	jal printInt
	jal printNewline

	# print product
	la $a0,productIs
	jal printString
	move $a0,$s3
	jal printInt
	jal printNewline

	# print exp-result
	move $a0,$s0
	jal printInt
	la $a0,toThe
	jal printString
	move $a0,$s1
	jal printInt
	la $a0,powerIs
	jal printString
	move $a0,$s4
	jal printInt
	jal printNewline

	# exit
	jal exit

#================ exit ================
exit:
	li $v0,10
	syscall
	jr $ra # should never get here

#================ printInt ================
printInt:
	li $v0,1
	syscall
	jr $ra

#================ printString ================
printString:
	li $v0,4
	syscall
	jr $ra

#================ printNewline ================
printNewline:
	li $v0,4
	li $a0,newlineString
	syscall
	jr $ra

#================ readNum ================
readNum:
	# read number and return
	li $v0,5
	syscall
	jr $ra


	.data

	# stringliterals
pleaseTypeFirst:
	.asciiz "Please type the first number: "
pleaseTypeSecond:
	.asciiz "Please type the second number: "
sumIs:
	.asciiz "The sum is "
productIs:
	.asciiz "The product is "
toThe:
	.asciiz " to the "
powerIs:
	.asciiz " power is "
newlineString:
	.asciiz "\n"
