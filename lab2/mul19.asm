	.data
msg:	.asciiz "select integer: "
end:	.asciiz "multiplied by 19 is: "
nln:	.asciiz "\n"


	.text
main:

	li $v0,4
	la $a0,msg
	syscall

	# get input val
	li $v0,5
	syscall

	move $t0,$v0
	move $t1,$v0
	# x << n + x << m
	# 2*(n+m) = 19
	# 19 = 16 + 3
	# x << 8 + x+x+x

	sll $t2,$t0,4
	add $t1,$t0
	add $t1,$t0
	#add $t1,$t0
	add $t2,$t2,$t1
	
	li $v0,4
	la $a0,end
	syscall
	
	li $v0,4
	move $a0,$t2
	syscall
	
	li $v0,4
	la $a0,nln
	syscall

	#exit the program
	li $v0,10 # syscall-opcode for "exit program"
	syscall
