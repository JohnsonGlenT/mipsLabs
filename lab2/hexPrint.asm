	.data
msg:	.asciiz "select integer: "
end:	.asciiz "in hex is: "
nln:	.asciiz "\n"
hex:	.ascii "0123456789ABCDEF"

	.text
main:

	li $v0,4
	la $a0,msg
	syscall

	# get input val
	li $v0,5
	syscall

	move $t1,$v0

	sll $t2,$t1,0
	sll $t3,$t1,20
	sll $t4,$t1,24
	sll $t5,$t1,28

	srl $t6,$t2,28
	srl $t7,$t3,28
	srl $t8,$t4,28
	srl $t9,$t5,28

	lb $t2,hex($t6)
	lb $t3,hex($t7)
	lb $t4,hex($t8)
	lb $t5,hex($t9)

	li $v0,4
	la $a0,end
	syscall

	li $v0,11
	move $a0,$t2
	syscall

	li $v0,11
	move $a0,$t3
	syscall

	li $v0,11
	move $a0,$t4
	syscall

	li $v0,11
	move $a0,$t5
	syscall

	#print one character at a time repeted systcalls

	li $v0,4
	la $a0,nln
	syscall

	li $v0,10 # syscall-opcode for "exit program"
	syscall
