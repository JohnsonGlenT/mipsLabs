	.data
msg:	.asciiz "select integer: "
end:	.asciiz "new val is: "
nln:	.asciiz "\n"

	.text
main:
	li $v0,4
	la $a0,msg
	syscall

	# get input val
	li $v0,5
	syscall
	move $t0,$v0

	li $v0,4
	la $a0,nln
	syscall

	li $v0,4
	la $a0,msg
	syscall

	# get input val
	li $v0,5
	syscall
	move $t1,$v0

	li $v0,4
	la $a0,nln
	syscall

	#first 8 from 2nd
	sll $t9,$t1,24

	srl $t8,$t0,18 #high bits
	sll $t7,$t0,22 #low bits

	li $t0,0
	srl $t1,$t7,22
	srl $t2,$t9,14
	sll $t3,$t8,18

	add $t0,$t1,$t2
	add $t0,$t0,$t3

	li $v0,4
	la $a0,end
	syscall

	li $v0,1
	move $a0,$t0
	syscall

	li $v0,4
	la $a0,nln
	syscall

	li $v0,10 # syscall-opcode for "exit program"
	syscall
