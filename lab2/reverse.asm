	.text
main:

	li $v0,4
	la $a0,msg
	syscall

	li $v0,5
	syscall
	move $t1,$v0

	li $v0,4
	la $a0,nln
	syscall

	li $t0,0
	li $t3,0
	li $t7,0
	li $t8,17

loop:

	srlv $t4,$t1,$t7
	sllv $t5,$t1,$t7

	sll $t4,$t4,31
	srl $t5,$t5,31

	srlv $t4,$t4,$t7
	sllv $t5,$t5,$t7

	add $t0,$t0,$t4
	add $t0,$t0,$t5

	add $t7,1
	beq $t7,$t8,done
	bne $t7,$t8,loop

done:

	li $v0,4
	la $a0,end
	syscall

	li $v0,1
	move $a0,$t0
	syscall

	li $v0,4
	la $a0,nln
	syscall

	#exit the program
	li $v0,10 # syscall-opcode for "exit program"
	syscall


	.data
msg:	.asciiz "select integer: "
end:	.asciiz "in revese binary is: "
nln:	.asciiz "\n"
hex:	.ascii "0123456789ABCDEF"

