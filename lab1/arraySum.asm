	.data
	#### define the array ####
abc: # label for beginning of array
	# elements in the array
	.word 30,-3,37,2876,28,7,-4,-73,-72,76
	.word 4,65,898,225,773,67437,5523,53,2256
	.word -44,2,77,2,774,2254,5287,222687,3235
abcEnd: # label for end of array

msg:	.asciiz "Sum is "
newline:
	.asciiz "\n"
	
	.text
main:		
	
	li $t0,0 # total
	li $t1,0 # arrayslot
	la $t2,abc # array
	la $t3,abcEnd

loop:
	lw $t1,0($t2)
	beq $t2,$t3,end
	add $t0,$t0,$t1
	add $t2,$t2,4
	bne $t2,$t3,loop

end:
	# print string part of our messsage
	li $v0,4
	la $a0,msg
	syscall

	# print the sum
	li $v0,1
	move $a0,$t0
	syscall

	# print a newline
	li $v0,4
	la $a0,newline
	syscall

 	# exit the program
 	li $v0,10
 	syscall
		