		.text
main:		
	
	lw $t1,abc
	lw $t2,def
	add $t0,$t1,$t2
	
	# print string part of our messsage
	li $v0,4
	la $a0,msg
	syscall

	# print the sum
	li $v0,1
	move $a0,$t0
	syscall

	# print a newline
	li $v0,4
	la $a0,newline
	syscall

 	# exit the program
 	li $v0,10
 	syscall

	.data
	#### define two data values
abc:	.word 337
def:	.word 8765
msg:	.asciiz "Sum is "
newline:
	.asciiz "\n"

	
