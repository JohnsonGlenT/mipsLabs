	.data
	# the string we're downcasing
myString:	.asciiz "Forget ME NOT, you wayward Foes!!!"
nln:	.asciiz "\n"
msg:	.asciiz "The new string is"

	.text
main:

	li $t0,0
	li $t1,0
	li $t2,0x41
	li $t3,0x5a
	li $t4,0x20
	li $t5,0
	
	la $t0,myString
	la $t5,nln
	lb $t1,0($t0)
	
loop:

	blt $t1,$t2,next
	bgt $t1,$t3,next
	add $t1,$t1,$t4
	sb $t1,0($t0)
	add $t0,$t0,1
	lb $t1,0($t0)
	beq $t0,$t5,end
	bne $t0,$t5,loop
	
next:

	add $t0,$t0,1
	lb $t1,0($t0)
	beq $t0,$t5,end
	bne $t0,$t5,loop
	
end:

	# print string part of our messsage
	li $v0,4
	la $a0,msg
	syscall

	# print a newline
	li $v0,4
	la $a0,nln
	syscall

	# print the new string
	li $v0,4
	la $a0,myString
	syscall

	# print a newline
	li $v0,4
	la $a0,nln
	syscall

	# exit the program
	li $v0,10
	syscall
