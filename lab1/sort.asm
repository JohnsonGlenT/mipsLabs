	.text
main:		
######## your code goes here ####
	li $t0,0 # smallest
	li $t1,0 # arrayslot
	la $t2,abc # array
	la $t3,abcEnd
	la $t4,abc # array
	li $t5,0
	lw $t0,0($t2)

loop:

	lw $t1,0($t2)
	ble $t1,$t0,sml
	add $t2,$t2,4
	beq $t2,$t3,nloop
	bne $t2,$t3,loop
	
sml:

	move $t0,$t1
	move $t5,$t2
	add $t2,$t2,4
	beq $t2,$t3,nloop
	beq $zero,$zero,loop

nloop:
	
	lw $t1,0($t4)
	sw $t0,0($t4)
	sw $t1,0($t5)
	add $t4,$t4,4
	li $t0,0 # smallest
	li $t1,0 # arrayslot
	move $t2,$t4 
	lw $t0,0($t2)
	beq $t4,$3,end
	bne $t4,$t3,loop

######### The remaining code prints the array elements  ####
######### and then exits the program.                   ####
end:
	la $t0,abc
	la $t1,abcEnd
printLoop:	
	li $v0,1 # code for print-integer
	lw $a0,($t0) # memory-value to print
	syscall
	li $v0,4 # code for print-string
	la $a0,newlineString # string with newline
	syscall
	addu $t0,$t0,4
	bne $t0,$t1,printLoop # loop back if not at end
	li $v0,10 # code for exit-program
	syscall

	.data
abc: # label for beginning of array
	# elements in the array
	.word 30,-3,37,2876,28,7,-4,-73,-72,76
	.word 4,65,898,225,773,67437,5523,53,2256
	.word -44,2,77,2,774,2254,5287,222687,3235
abcEnd: # label for end of array
	.word -999999 # dummy word
newlineString:
	.asciiz "\n"
