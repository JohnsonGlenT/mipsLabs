	.text
	
	################################################################
	# beginning of main program
	################################################################
main:	
	# print first prompt
	li $v0,4
	la $a0,prompt1
	syscall

	# read first string into buffer and wipe out newline character
	li $v0,8
	la $a0,buffer1
	li $a1,1000
	syscall
	jal killNLChar
	
	# print secondprompt
	li $v0,4
	la $a0,prompt2
	syscall

	# read second string into buffer and wipe out newline character
	li $v0,8
	la $a0,buffer2
	li $a1,1000
	syscall
	jal killNLChar

	# call the 'strCmp' function
	la $a0, buffer1
	la $a1, buffer2
	jal strCmp

	# save result in $t0
	move $t0,$v0

	# print the result
	li $v0,4
	la $a0,resultIs
	syscall
	li $v0,1
	move $a0,$t0
	syscall

	# print newline
	li $v0,4
	la $a0,newline
	syscall
	
	# exit
	li $v0,10
	syscall
	################################################################
	# end of main program
	################################################################
	

	
	################################################################
	# beginning of killNLChar - function to kill newline character
	################################################################
killNLChar:	
	move $t0,$a0
	b knlcEnter
knlcLoop:
	addu $t0,$t0,1	
knlcEnter:	
	lb $t1,($t0)
	beqz $t1,knlcEnd
	subu $t1,'\n'
	bnez $t1,knlcLoop
	sb $zero,($t0)
	
knlcEnd:	
	jr $ra
	################################################################
	# end of killNLChar - function to kill newline character
	################################################################

	
	################################################################
	# beginning of strCmp function, which you should modify
	################################################################
strCmp:

	lb $t0,0($a0)
	lb $t1,0($a1)
	li $t2,0
	li $t3,0
	li $t4,0

	lenLoop:
		lb $t0,0($a0)
		lb $t1,0($a1)
		beqz $t0,check1
		beqz $t1,check2
		addi $a0,$a0,1
		addi $a1,$a1,1
		addi $t3,$t3,1
		addi $t4,$t4,1
		j lenLoop


	check1:
		beqz $t1,endLoop
		j add1
	check2:
		beqz $t0,endLoop
		j add2
	add1:
		addi $t4,$t4,1
		j endLoop
	add2:
		addi $t3,$t3,1
		j endLoop

	endLoop:
	blt $t3,$t4,return0
	beq $t3,$t4,return1
	bgt $t3,$t4,return2

	# return from function
return0:
	li $v0,-1
	jr $ra

return1:
	li $v0,0
	jr $ra

return2:
	li $v0,1
	jr $ra

	################################################################
	# end of strCmp function
	################################################################

	.data
buffer1:
	.space 1000
buffer2:
	.space 1000

prompt1:	
	.asciiz "Please type in a string: "
prompt2:	
	.asciiz "Please type in another string: "
newline:
	.asciiz "\n"
resultIs:
	.asciiz "The result is "
