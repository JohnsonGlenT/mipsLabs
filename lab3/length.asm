	.text

	################################################################
	# beginning of main program
	################################################################
main:
	# print prompt
	li $v0,4
	la $a0,prompt1
	syscall

	# read string into buffer and wipe out newline character
	li $v0,8
	la $a0,buffer
	li $a1,1000
	syscall
	jal killNLChar

	# call the 'strLen' function
	la $a0,buffer
	jal strLen

	# save result in $t0
	move $t0,$v0

	# print the result
	li $v0,4
	la $a0,resultIs
	syscall
	li $v0,1
	move $a0,$t0
	syscall

	# print newline
	li $v0,4
	la $a0,newline
	syscall

	# exit
	li $v0,10
	syscall
	################################################################
	# end of main program
	################################################################


	################################################################
	# beginning of killNLChar - function to kill newline character
	################################################################
killNLChar:
	move $t0,$a0
	b knlcEnter
knlcLoop:
	addu $t0,$t0,1
knlcEnter:
	lb $t1,($t0)
	beqz $t1,knlcEnd
	subu $t1,'\n'
	bnez $t1,knlcLoop
	sb $zero,($t0)

knlcEnd:
	jr $ra
	################################################################
	# end of killNLChar - function to kill newline character
	################################################################

	################################################################
	# beginning of strLen function, which you should modify
	################################################################
strLen:
	# get value from register
	lb $t1,0($a0)
	li $t0,0

lenLoop:
	beqz $t1,lenEnd
	addi $t0,$t0,1
	addi $t1,$t1,1
	bnez $t1,lenLoop

lenEnd:
	# return from function
	jr $ra
	################################################################
	# end of strLen function
	################################################################

	.data
buffer:
	.space 1000

prompt1:
	.asciiz "Please type in a string: "
newline:
	.asciiz "\n"
resultIs:
	.asciiz "The result is "
