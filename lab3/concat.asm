	.text
	
	################################################################
	# beginning of main program
	################################################################
main:	
	# print first prompt
	li $v0,4
	la $a0,prompt1
	syscall

	# read first string into buffer and wipe out newline character
	li $v0,8
	la $a0,buffer1
	li $a1,1000
	syscall
	jal killNLChar
	
	# print secondprompt
	li $v0,4
	la $a0,prompt2
	syscall

	# read second string into buffer and wipe out newline character
	li $v0,8
	la $a0,buffer2
	li $a1,1000
	syscall
	jal killNLChar

	# call the 'concat' function
	la $a0, buffer1
	la $a1, buffer2
	la $a2, buffer3
	jal concat

	# print the result
	li $v0,4
	la $a0,resultIs
	syscall

	li $v0,4
	la $a0,buffer3
	syscall

	# print newline
	li $v0,4
	la $a0,newline
	syscall

	# exit
	li $v0,10
	syscall
	################################################################
	# end of main program
	################################################################
	
	################################################################
	# beginning of killNLChar - function to kill newline character
	################################################################
killNLChar:	
	move $t0,$a0
	b knlcEnter
knlcLoop:
	addu $t0,$t0,1	
knlcEnter:	
	lb $t1,($t0)
	beqz $t1,knlcEnd
	subu $t1,'\n'
	bnez $t1,knlcLoop
	sb $zero,($t0)
	
knlcEnd:	
	jr $ra
	################################################################
	# end of killNLChar - function to kill newline character
	################################################################
	

	
	################################################################
	# beginning of concat function, which you should modify
	################################################################
concat:
	lb $t0,0($a0) #buffer 1
	lb $t1,0($a1) #buffer 2
	lb $t2,0($a2) #buffer 3

	prg1:
		lb $t0,0($a0)
		beqz $t0,prg2
		sb $t0,0($a2)
		addi $a0,1
		addi $a2,1
		j prg1

	prg2:
		lb $t1,0($a1)
		sb $t1,0($a2)
		beqz $t1,return
		addi $a1,1
		addi $a2,1
		j prg2

return:
	jr $ra
	################################################################
	# end of concat function
	################################################################

	.data
buffer1:
	.space 1000
buffer2:
	.space 1000
buffer3:
	.asciiz "This is some initial text for the buffer."
	.space 2000

prompt1:	
	.asciiz "Please type in a string: "
prompt2:	
	.asciiz "Please type in another string: "
newline:
	.asciiz "\n"
resultIs:
	.asciiz "The result is\n -->"
