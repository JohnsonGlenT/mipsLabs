	.text
	
	################################################################
	# beginning of main program
	################################################################
main:	
	# print first prompt
	li $v0,4
	la $a0,prompt1
	syscall

	# read string into buffer and wipe out newline character
	li $v0,8
	la $a0,buffer1
	li $a1,1000
	syscall
	jal killNLChar

	# print second prompt
	li $v0,4
	la $a0,prompt2
	syscall

	# read first integer
	li $v0,5
	syscall
	move $t0,$v0 # save in $t0
	
	# print third prompt
	li $v0,4
	la $a0,prompt3
	syscall

	# read second integer
	li $v0,5
	syscall

	# call the 'substring' function
	la $a0, buffer1
	la $a1, buffer2
	move $a2,$t0
	move $a3,$v0
	jal substring

	# print the result
	li $v0,4
	la $a0,resultIs
	syscall

	li $v0,4
	la $a0,buffer2
	syscall

	# print newline
	li $v0,4
	la $a0,newline
	syscall
	
	# exit
	li $v0,10
	syscall
	################################################################
	# end of main program
	################################################################
	
	
	################################################################
	# beginning of killNLChar - function to kill newline character
	################################################################
killNLChar:	
	move $t0,$a0
	b knlcEnter
knlcLoop:
	addu $t0,$t0,1	
knlcEnter:	
	lb $t1,($t0)
	beqz $t1,knlcEnd
	subu $t1,'\n'
	bnez $t1,knlcLoop
	sb $zero,($t0)
	
knlcEnd:	
	jr $ra
	################################################################
	# end of killNLChar - function to kill newline character
	################################################################

	
	################################################################
	# beginning of substring function, which you should modify
	################################################################
substring:

	move $t0,$a2
	move $t1,$a3

	add $a0,$a0,$t0

	jmp:

	lb $t4,0($a0)

	beq $t4,$zero,return
	blt $t1,$t0,return

	sb $t4,0($a1)

	addi $a0,$a0,1
	addi $a1,$a1,1
	addi $t0,$t0,1

	j jmp

	return:
	sb $zero,0($a1)
	jr $ra
	################################################################
	# end of substring function
	################################################################

	.data
buffer1:
	.space 1000
buffer2:
	.asciiz "This is some initial text for the buffer."
	.space 1000

prompt1:	
	.asciiz "Please type in a string: "
prompt2:	
	.asciiz "Please type the start-index: "
prompt3:	
	.asciiz "Please type the end-index: "
newline:
	.asciiz "\n"
resultIs:
	.asciiz "The substring is\n -->"
