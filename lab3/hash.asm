	.text
	
	################################################################
	# beginning of main program
	################################################################
main:	
	# print prompt
	li $v0,4
	la $a0,prompt1
	syscall

	# read string into buffer and wipe out newline character
	li $v0,8
	la $a0,buffer
	li $a1,1000
	syscall
	jal killNLChar

	# call the 'hash' function
	la $a0,buffer
	jal hash

	# save result in $t0
	move $t0,$v0

	# print the result
	li $v0,4
	la $a0,resultIs
	syscall
	li $v0,1
	move $a0,$t0
	syscall

	# print newline
	li $v0,4
	la $a0,newline
	syscall
	
	# exit
	li $v0,10
	syscall
	################################################################
	# end of main program
	################################################################
	
	
	################################################################
	# beginning of killNLChar - function to kill newline character
	################################################################
killNLChar:	
	move $t0,$a0
	b knlcEnter
knlcLoop:
	addu $t0,$t0,1	
knlcEnter:	
	lb $t1,($t0)
	beqz $t1,knlcEnd
	subu $t1,'\n'
	bnez $t1,knlcLoop
	sb $zero,($t0)
	
knlcEnd:	
	jr $ra
	################################################################
	# end of killNLChar - function to kill newline character
	################################################################



	
	################################################################
	# beginning of hash function, which you should modify
	################################################################
hash:
	li $t1,11653
next:
	lb $t0,0($a0)

	beqz $t0,end

	sll $t4,$t1,3
	add $t4,$t4,$t1
	add $t4,$t4,$t1
	add $t4,$t4,$t1
	add $t4,$t4,$t1
	add $t4,$t4,$t1
	add $t4,$t4,$t0
	addi $a0,1

	#sb $t0,0($t4)

	move $t1,$t4
	j next
end:
	move $v0,$t4
	jr $ra
	################################################################
	# end of hash function
	################################################################

	.data
buffer:
	.space 1000

prompt1:	
	.asciiz "Please type in a string: "
newline:
	.asciiz "\n"
resultIs:
	.asciiz "The result is "
