	.text
main:

	move $t9,$sp
	subu $sp,$sp,4

	readInt:

		li $v0,4
		la $a0,prompt1
		syscall

		#gets int until
		li $v0,5
		syscall

		move $t0,$v0
		blt $t0,$zero, popPre

		sw $v0,0($sp)

		li $v0,4
		la $a0,nline
		syscall

		subu $sp,$sp,4

		j readInt

	popPre:

		addu $sp,$sp,4

	pop:

		lw $a0,0($sp)
		addu $sp,$sp,4

		li $v0,1
		syscall

		li $v0,4
		la $a0,nline
		syscall

		beq $t9,$sp,end
		j pop

end:

	# exit
	li $v0,10
	syscall

	.data
prompt1:
	.asciiz "Enter an Int: "
nline:
	.asciiz "\n"
