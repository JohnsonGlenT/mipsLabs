	.text
main:

	li $t9,4
	li $t8,0

	lw $t0,firstElement($zero)
	lw $t1,firstElement($t9)

	print:

		li $v0,1
		move $a0,$t0
		add $t8,$t8,$t0
		syscall

		li $v0,4
		la $a0,nline
		syscall

		beq $t1,$zero,exit
		j loop

	loop:
		lw $t0,0($t1)
		lw $t1,4($t1)
		j print


exit:

	li $v0,1
	move $a0,$t8
	syscall

	li $v0,4
	la $a0,nline
	syscall


	# exit
	li $v0,10
	syscall

	.data

	# statically-allocated linked list
	.align 2
node1:	.word -5,node3
node2:	.word 17,0
node3:	.word 44,node4
node4:	.word 6,node5
firstElement:
	.word -9,node1
node5:	.word -21,node2
nline:	.asciiz "\n"
