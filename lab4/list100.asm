	.text
main:

	li $t9,1
	li $t8,101

	li $a0,8
	jal malloc

	move $t3,$v0
	#head pointer

	sw $t9,0($v0)
	sw $t0,4($v0)

	addi $t9,$t9,1

loop:

	li $a0,8
	jal malloc

	# $t0 next allocation
	# $v0 current allocation
	sw $t9,0($v0)
	sw $t0,4($v0)

	addi $t9,$t9,1

	move $t4,$t0 #end
	beq $t9,$t8,print

	j loop

print:

	lw $t0,0($t3)
	lw $t3,4($t3)

	li $v0,1
	move $a0,$t0
	syscall

	li $v0,4
	la $a0,nline
	syscall

	beq $t3,$t4,exit

	j print


exit:

	# exit
	li $v0,10
	syscall

########### don't touch the code below--it handles heap allocation ##########
	.data
	# ensure that we're word-aligned
	.align 2

	# word to keep track of next available heap location
currentHeapPointer:
	.word heapStart

	# the heap space
heapStart:
	.space 100000
heapEnd:
nline:	.asciiz "\n"

	.text
	# the heap-allocation function
malloc:
	# load return value into $v0
	lw $v0,currentHeapPointer

	# bump heap pointer by byte-size object, rounded up to next
	# multiple of 4
	addu $t0,$v0,$a0
	addu $t0,$t0,3
	and $t0,$t0,0xfffffffc

	# store heap pointer for next allocation
	sw $t0,currentHeapPointer

	#return
	jr $ra
