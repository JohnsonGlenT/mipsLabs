	.text
main:

	li $v0,4
	la $a0,prompt1
	syscall

	#gets int until
	li $v0,5
	syscall

	move $t1,$v0

	li $v0,4
	la $a0,nline
	syscall

	li $a0,8
	jal malloc

	move $t3,$v0
	move $t4,$t0
	#head pointer

	sw $t1,0($v0)
	sw $t0,4($v0)

	beq $t1,$zero,print

loop:

	li $v0,4
	la $a0,prompt1
	syscall

	#gets int until
	li $v0,5
	syscall

	move $t1,$v0

	li $v0,4
	la $a0,nline
	syscall

	li $a0,8
	jal malloc

	# $t0 next allocation
	# $v0 current allocation
	sw $t1,0($v0)
	sw $t0,4($v0)

	move $t4,$t0 #end
	beq $t1,$zero,print

	j loop

print:

	li $v0,4
	la $a0,sep
	syscall

	li $v0,4
	la $a0,nline
	syscall

	move $t2,$t3

print1:

	lw $t0,0($t2)
	lw $t2,4($t2)

	li $v0,1
	move $a0,$t0
	syscall

	li $v0,4
	la $a0,nline
	syscall

	beq $t2,$t4,prePush

	j print1

prePush:

	move $t9,$sp
	subu $sp,$sp,4
	move $t2,$t3

	li $v0,4
	la $a0,sep
	syscall

	li $v0,4
	la $a0,nline
	syscall


push:

	lw $t1,0($t2)
	lw $t2,4($t2)

	sw $t1,0($sp)

	subu $sp,$sp,4

	beq $t2,$t4, popPre

	j push

popPre:

	addu $sp,$sp,4

pop:

	lw $a0,0($sp)
	addu $sp,$sp,4

	li $v0,1
	syscall

	li $v0,4
	la $a0,nline
	syscall

	beq $t9,$sp,exit
	j pop

exit:

	# exit
	li $v0,10
	syscall

	########### don't touch the code below--it handles heap allocation ##########
	.data

prompt1:
	.asciiz "Enter an Int: "
nline:
	.asciiz "\n"
sep:
	.asciiz "----------"

	# ensure that we're word-aligned
	.align 2

	# word to keep track of next available heap location
currentHeapPointer:
	.word heapStart

	# the heap space
heapStart:
	.space 100000
heapEnd:

	.text
	# the heap-allocation function
malloc:
	# load return value into $v0
	lw $v0,currentHeapPointer

	# bump heap pointer by byte-size object, rounded up to next
	# multiple of 4
	addu $t0,$v0,$a0
	addu $t0,$t0,3
	and $t0,$t0,0xfffffffc

	# store heap pointer for next allocation
	sw $t0,currentHeapPointer

	#return
	jr $ra
